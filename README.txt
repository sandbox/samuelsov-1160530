This module allow to add desjardins gateway to ubercart.


Installation
------------
1) Copy the uc_desjardins directory to the modules folder or to modules/ubercart/contrib.

2) Enable the module using Administer -> Modules (/admin/build/modules)



Configuration
-------------
1) Go to Store administration › Configuration › Payment settings and fill the information

2) Configure conditionnal action to send uc_order-customer-desj.tpl.php template instead of customer.tpl.php.

The template contains information about Credit card transaction needed for desjardins.
